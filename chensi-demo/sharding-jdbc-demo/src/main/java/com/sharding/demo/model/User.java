package com.sharding.demo.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chensi.common.model.SuperEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author si.chen
 * @date 2020/8/16 14:11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("user")
public class User extends SuperEntity {
    private static final long serialVersionUID = -4543945536405976559L;
    private String companyId;
    private String name;
}
