package com.sharding.demo.mapper;

import com.chensi.db.mapper.SuperMapper;
import com.sharding.demo.model.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author si.chen
 * @date 2020/8/16 14:12
 */
@Mapper
public interface UserMapper extends SuperMapper<User> {
}
