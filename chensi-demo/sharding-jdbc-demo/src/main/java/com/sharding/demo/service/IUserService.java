package com.sharding.demo.service;

import com.chensi.common.service.ISuperService;
import com.sharding.demo.model.User;

/**
 * @author si.chen
 * @date 2020/8/16 14:13
 */
public interface IUserService extends ISuperService<User> {
}
