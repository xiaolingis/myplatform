package org.txlcn.demo.common.spring;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author si.chen
 * @date 2020/8/18 18:25
 */
@FeignClient(value = "txlcn-demo-spring-service-b")
public interface ServiceBClient {
    @GetMapping("/rpc")
    String rpc(@RequestParam("value") String name);
}
