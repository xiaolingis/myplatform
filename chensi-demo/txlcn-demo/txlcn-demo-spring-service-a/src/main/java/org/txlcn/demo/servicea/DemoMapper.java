package org.txlcn.demo.servicea;

import org.apache.ibatis.annotations.Mapper;
import org.txlcn.demo.common.db.mapper.BaseDemoMapper;

/**
 * @author si.chen
 * @date 2020/8/18 18:31
 */
@Mapper
public interface DemoMapper extends BaseDemoMapper {
}
