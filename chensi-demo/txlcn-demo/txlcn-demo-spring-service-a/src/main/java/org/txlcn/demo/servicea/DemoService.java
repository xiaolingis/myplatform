package org.txlcn.demo.servicea;

/**
 * @author si.chen
 * @date 2020/8/18 18:32
 */
public interface DemoService {
    String execute(String value, String ex, String flag);
}
