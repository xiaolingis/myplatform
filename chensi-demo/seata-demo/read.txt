## **详细的原理和部署细节请查看**

[Spring Cloud同步场景分布式事务怎样做？试试Seata](https://mp.weixin.qq.com/s/0yCmHzlXDC9BkbUuEt0_fQ)



## 说明


**包括以下5个模块，分别是**

* `business-service`：业务服务
* `storage-service`：库存服务
* `order-service`：订单服务
* `account-service`：账号服务
* `seata-common-starter`：公共工程

&nbsp;
**本demo主要是模拟用户下订单的场景，整个流程如下：**
用户下单(`business-service`) -> 扣库存(`storage-service`) -> 创建订单(`order-service`) -> 减少账户余额(`account-service`)

&nbsp;
**提供以下两个测试接口**

1. 事务成功：扣除库存成功 > 创建订单成功 > 扣减账户余额成功
   http://localhost:9090/placeOrder
1. 事务失败：扣除库存成功 > 创建订单成功 > 扣减账户余额失败，事务回滚
   http://localhost:9090/placeOrderFallBack


注意：

seata所有涉及的中间件配置注册中心必须是局域网地址192.168.32.1，而不能是127.0.0.1

seata的jar包替换alibaba-seata里面的seata版本要改成1.3.0
   <dependency>
               <groupId>com.alibaba.cloud</groupId>
               <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
               <version>2.2.1.RELEASE</version>
               <exclusions>
                   <exclusion>
                       <groupId>io.seata</groupId>
                       <artifactId>seata-all</artifactId>
                   </exclusion>
                   <exclusion>
                       <groupId>io.seata</groupId>
                       <artifactId>seata-spring-boot-starter</artifactId>
                   </exclusion>
               </exclusions>
           </dependency>
           <dependency>
               <groupId>io.seata</groupId>
               <artifactId>seata-spring-boot-starter</artifactId>
               <version>1.3.0</version>
           </dependency>