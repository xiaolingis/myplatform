package com.chensi.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chensi.account.model.Account;

/**
 * @author si.chen
 * @date 2020/8/22 16:26
 */
public interface AccountMapper extends BaseMapper<Account> {
}
