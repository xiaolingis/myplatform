package com.chensi.storage.mapper;

import com.chensi.db.mapper.SuperMapper;
import com.chensi.storage.model.Storage;

/**
 * @author si.chen
 * @date 2020/8/22 16:13
 */
public interface StorageMapper extends SuperMapper<Storage> {
}
