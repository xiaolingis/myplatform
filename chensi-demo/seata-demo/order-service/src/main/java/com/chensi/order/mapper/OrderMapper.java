package com.chensi.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chensi.order.model.Order;

/**
 * @author si.chen
 * @date 2020/8/22 16:20
 */
public interface OrderMapper extends BaseMapper<Order> {
}
