package com.chensi.business.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author si.chen
 * @date 2020/8/22 16:00
 */
@FeignClient(name = "order-service")
public interface OrderFeignClient {
    @GetMapping("order/create")
    Boolean create(@RequestParam("userId") String userId,
                   @RequestParam("commodityCode") String commodityCode,
                   @RequestParam("count") Integer count);
}
