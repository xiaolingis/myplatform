package com.rocketmq.demo.service;

import com.rocketmq.demo.model.Order;

/**
 * @author si.chen
 * @date 2020/8/17 10:01
 */
public interface IOrderService {
    void save(Order order);
}
