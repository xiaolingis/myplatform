package com.rocketmq.demo.service.impl;

import com.rocketmq.demo.model.Order;
import com.rocketmq.demo.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author si.chen
 * @date 2020/8/17 10:01
 */
@Slf4j
@Service
public class OrderServiceImpl implements IOrderService {
    @Override
    public void save(Order order) {
        System.out.println("============保存订单成功：" + order.getOrderId());
    }
}
