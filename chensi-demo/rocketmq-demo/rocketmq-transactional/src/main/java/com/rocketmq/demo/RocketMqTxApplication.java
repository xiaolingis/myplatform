package com.rocketmq.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author si.chen
 * @date 2020/8/17 14:47
 */
@EnableTransactionManagement
@SpringBootApplication
public class RocketMqTxApplication {
    public static void main(String[] args) {
        SpringApplication.run(RocketMqTxApplication.class, args);
    }
}
