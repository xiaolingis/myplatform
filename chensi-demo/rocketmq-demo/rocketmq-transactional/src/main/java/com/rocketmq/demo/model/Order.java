package com.rocketmq.demo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author si.chen
 * @date 2020/8/17 9:59
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Order implements Serializable {
    private static final long serialVersionUID = 1253948452208537350L;
    private Long orderId;
    private String orderNo;
}
