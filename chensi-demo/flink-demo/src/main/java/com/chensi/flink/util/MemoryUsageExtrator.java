package com.chensi.flink.util;

import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;

/**
 * 提取当前可用内存字节数
 *
 * @author si.chen
 * @date 2020/8/29 13:38
 */
public class MemoryUsageExtrator {
    private static OperatingSystemMXBean mxBean =
            (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();

    /**
     * Get current free memory size in bytes
     *
     * @return free RAM size
     */
    public static long currentFreeMemorySizeInBytes() {
        OperatingSystemMXBean osmxb = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
        return osmxb.getFreePhysicalMemorySize();
    }
}
