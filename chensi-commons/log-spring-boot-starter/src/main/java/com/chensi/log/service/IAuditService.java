package com.chensi.log.service;

import com.chensi.log.model.Audit;

/**
 * 审计日志接口
 *
 * @author si.chen
 * @date 2020/8/15 9:46
 */
public interface IAuditService {
    void save(Audit audit);
}
