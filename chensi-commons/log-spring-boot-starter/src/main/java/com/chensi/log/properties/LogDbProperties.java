package com.chensi.log.properties;

import com.zaxxer.hikari.HikariConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 日志数据源配置
 * logType=db时生效(非必须)，如果不配置则使用当前数据源
 *
 * @author si.chen
 * @date 2020/8/14 12:39
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "chensi.audit-log.datasource")
public class LogDbProperties extends HikariConfig {
}
