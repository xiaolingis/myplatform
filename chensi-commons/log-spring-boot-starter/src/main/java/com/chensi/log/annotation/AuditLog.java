package com.chensi.log.annotation;

import java.lang.annotation.*;

/**
 * @author si.chen
 * @date 2020/8/14 12:31
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuditLog {
    /**
     * 操作信息
     */
    String operation();
}
