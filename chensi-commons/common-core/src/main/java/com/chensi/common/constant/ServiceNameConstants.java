package com.chensi.common.constant;

/**
 * 服务名称常量
 *
 * @author si.chen
 * @date 2020/8/15 19:52
 */
public interface ServiceNameConstants {
    /**
     * 用户权限服务
     */
    String USER_SERVICE = "user-center";
    /**
     * 搜索中心服务
     */
    String SEARCH_SERVICE = "search-center";
}
