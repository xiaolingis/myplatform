package com.chensi.common.exception;

/**
 * 分布式锁异常
 *
 * @author si.chen
 * @date 2020/8/15 19:40
 */
public class LockException extends RuntimeException {
    private static final long serialVersionUID = 2541934644377883738L;

    public LockException(String message) {
        super(message);
    }
}
