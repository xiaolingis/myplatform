package com.chensi.common.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色
 *
 * @author si.chen
 * @date 2020/8/15 19:26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
public class SysRole extends SuperEntity {
    private static final long serialVersionUID = 2962892906650680966L;
    private String code;
    private String name;
    @TableField(exist = false)
    private Long userId;
}
