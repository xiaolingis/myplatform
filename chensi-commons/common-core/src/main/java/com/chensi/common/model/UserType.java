package com.chensi.common.model;

/**
 * 用户类型
 *
 * @author si.chen
 * @date 2020/8/15 19:34
 */
public enum UserType {
    /**
     * 前端app用户
     */
    APP,
    /**
     * 后端管理用户
     */
    BACKEND
}
