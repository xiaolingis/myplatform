package com.chensi.common.constant;

/**
 * 配置项常量
 *
 * @author si.chen
 * @date 2020/8/15 19:51
 */
public interface ConfigConstants {
    /**
     * 是否开启自定义隔离规则
     */
    String CONFIG_RIBBON_ISOLATION_ENABLED = "chensi.ribbon.isolation.enabled";
}
