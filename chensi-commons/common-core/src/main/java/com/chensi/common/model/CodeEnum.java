package com.chensi.common.model;

/**
 * @author si.chen
 * @date 2020/8/15 19:19
 */
public enum CodeEnum {

    SUCCESS(0),
    ERROR(1);

    private Integer code;

    CodeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
