package com.chensi.common.exception;

/**
 * 幂等性异常
 *
 * @author si.chen
 * @date 2020/8/15 19:39
 */
public class IdempotencyException extends RuntimeException {
    private static final long serialVersionUID = 4769998845020733057L;

    public IdempotencyException(String message) {
        super(message);
    }
}
