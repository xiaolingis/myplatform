package com.chensi.common.exception;

/**
 * @author si.chen
 * @date 2020/8/15 19:35
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 4286575239958141359L;

    public BusinessException(String message) {
        super(message);
    }
}
