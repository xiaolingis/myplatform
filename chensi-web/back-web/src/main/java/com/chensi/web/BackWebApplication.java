package com.chensi.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author si.chen
 * @date 2020/8/19 18:25
 */
@SpringBootApplication
public class BackWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackWebApplication.class, args);
    }
}
